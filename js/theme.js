/**
 * Created by Bosko Kuzmanovic on 6/28/2017.
 */

/*
Declare global atm object
*/
var atm = atm || {};
atm.userCoordinates = atm.userCoordinates || {};
atm.navigator = atm.navigator || {};
atm.places = atm.places || {};
atm.navigator.options = atm.navigator.options || {};
atm.locations = atm.locations || [];

/*
Set navigator options
 */
atm.navigator.options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

/*
Method to be called if user allows location usage
 */
atm.navigator.success = function (position) {
    atm.userCoordinates = new google.maps.LatLng(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude));
    atm.startMaping();
};

/*
Method to be called if user doesn't allow location usage or other error occurs
 */
atm.navigator.error = function (err) {
    this.textEl = document.createElement('h3');
    this.textEl.className = 'error-message';
    if(!err.code && !err.message) {
        this.textElText = document.createTextNode(err);
    }
    else {
        this.textElText = document.createTextNode('ERROR(' + err.code + '): ' + err.message);
    }
    this.textEl.appendChild(this.textElText);
    document.body.insertBefore(this.textEl, document.body.firstChild);
};

/*
Get users location
 */
function initMap() {
    navigator.geolocation.getCurrentPosition(atm.navigator.success, atm.navigator.error, atm.navigator.options);
}

/*
Initiate naerby Search
 */
atm.startMaping = function () {
    atm.places = new google.maps.places.PlacesService(document.createElement('div'));
    atm.places.nearbySearch({
        location: atm.userCoordinates,
        radius: 50000,
        type: ['atm'],
        rankby: 'distance',
        keyword: 'Telenor'
    }, atm.callback);
};

/*
Callback method to populate atm.locations array
 */
atm.callback = function (results, status) {
    if(results.length > 10) {
        atm.locationsCount = 10;
    }
    else if (results.length === 0) {
        var errorMessage = 'There are no ATM\'s around you';
        atm.navigator.error(errorMessage);
    }
    else {
        atm.locationsCount = results.length;
    }

    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < atm.locationsCount; i++) {
            results[i].atmCoordinates = new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng());
            results[i].distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(atm.userCoordinates, results[i].atmCoordinates));
            results[i].thumbUrl = atm.getPhotoImage(results[i].photos);
            atm.locations.push(results[i]);
        }
        atm.createHtml(true);
    }
    else if(status === google.maps.places.PlacesServiceStatus.INVALID_REQUEST) {
        atm.navigator.error('Required query parameter (location or radius) is missing');
    }
    else if(status === google.maps.places.PlacesServiceStatus.REQUEST_DENIED ) {
        atm.navigator.error('Request was denied, generally because of lack of an invalid key parameter');
    }
    else if(status === google.maps.places.PlacesServiceStatus.OVER_QUERY_LIMIT ) {
        atm.navigator.error('You are over your quota');
    }
    else if(status === google.maps.places.PlacesServiceStatus.ZERO_RESULTS ) {
        atm.navigator.error('The search was successful but returned no results. This may occur if the search was passed a latlng in a remote location');
    }
    else {
        atm.navigator.error('Something, who knows what, went terribly wrong');
    }
};

/*
Method for sorting ascending
 */
atm.locations.sortingAsc = function () {
    this.sort(function (a, b) {
        return a.distance - b.distance;
    });
};

/*
Method for sorting descending
 */
atm.locations.sortingDesc = function () {
    this.sort(function (a, b) {
        return b.distance - a.distance;
    });
};

/*
Get first photo url from photos array or use fallback image url
*/
atm.getPhotoImage = function(photos) {
    var getPhoto;
    if(!photos) {
        getPhoto = 'https://quantox.com/assets/img/about-pic-13.jpg"';
    }
    else {
        getPhoto = photos[0].getUrl({'maxWidth': '1500', 'maxHeight': '500'});
    }
    return getPhoto;
};

/*
Method for dynamically creating HTML
 */
atm.createHtml = function (asc) {
    var myNode = document.getElementById('atmResponse');
    while (myNode.lastChild) {
        myNode.removeChild(myNode.lastChild);
    }
    if(asc) {
        atm.locations.sortingAsc();
    }
    else {
        atm.locations.sortingDesc();
    }
    for(var i = 0; i < atm.locations.length; i++) {
        document.getElementById('atmResponse').innerHTML += '' +
            '<div class="row atm-element">' +
            '<div class="col-md-8">' +
            '<h2>Bank name: <br><span>' + atm.locations[i].name + '</span></h2>' +
            '<h4>Location: <br><span>' + atm.locations[i].vicinity + '</span></h4>' +
            '<h3>Distance from you: <br><span>' + atm.locations[i].distance + ' meters</span></h3>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="map-image" style="background-image: url(https://maps.googleapis.com/maps/api/staticmap?' +
            'center=' + atm.locations[i].atmCoordinates.lat() + ',' + atm.locations[i].atmCoordinates.lng() + '&' +
            'zoom=13&' +
            'scale=2&' +
            'size=330x330&' +
            'maptype=roadmap&' +
            'format=png&' +
            'visual_refresh=true&' +
            'key=AIzaSyDr7Y4ZgnxRhghOrltVx3ixzqBvqolJ_AE&' +
            'markers=size:mid%7Ccolor:0x96ca3a%7Clabel:1%7C' + atm.locations[i].atmCoordinates.lat() + ',' + atm.locations[i].atmCoordinates.lng() + '); ' +
            'background-position: center;">' +
            '<div class="directions-container" style="background-image: url(' + atm.locations[i].thumbUrl + ');" >' +
            '<a class="get-directions-btn" target="_blank" ' +
            'href="https://www.google.com/maps/dir//' + atm.locations[i].atmCoordinates.lat() + ',' + atm.locations[i].atmCoordinates.lng() + '/">' +
            'Get Directions' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    }
};