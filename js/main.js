/**
 * Created by Bosko Kuzmanovic on 6/29/2017.
 */

/*
main.js is used for traversing
*/
window.addEventListener('load', initialize);
function initialize() {
    var sortButtons = document.getElementsByClassName('sort-button');
    for(var i = 0; i < sortButtons.length; i++){
        sortButtons[i].addEventListener('click', function () {
            if(this.id === 'sortAsc') {
                atm.createHtml(true);
            }
            else {
                atm.createHtml(false);
            }
        });
    }
    function checkVerticalPosition() {
        var header = document.getElementsByTagName('header')[0];
        if(window.pageYOffset > 0) {
            header.classList.add('fixed-header');
        }
        else {
            header.classList.remove('fixed-header');
        }
    }
    checkVerticalPosition();
    window.addEventListener('scroll', function(e) {
        checkVerticalPosition();
    });
}